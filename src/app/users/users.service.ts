import { Injectable } from '@angular/core';

@Injectable()
export class UsersService {
  public users: any;

  constructor() {
    this.users = [
      { id: 1, dob: new Date(), greeting: 'hello', name: 'john'},
      { id: 2, dob: new Date(), greeting: 'hello', name: 'paul'},
      { id: 3, dob: new Date(), greeting: 'hello', name: 'jean'},
      { id: 4, dob: new Date(), greeting: 'hello', name: 'mark'},
      { id: 5, dob: new Date(), greeting: 'hello', name: 'mary'},
      { id: 6, dob: new Date(), greeting: 'hello', name: 'kate'},
      { id: 7, dob: new Date(), greeting: 'hello', name: 'hans'},
      { id: 8, dob: new Date(), greeting: 'hello', name: 'alex'},
    ];
  }

  getUser = (id) => this.users.filter(user => user.id === id)[0];
}
