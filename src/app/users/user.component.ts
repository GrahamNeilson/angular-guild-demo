import { Component, OnInit } from '@angular/core';

import { UsersService } from './users.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: `app-user`,
  styles: [`
    md-card-subtitle {
      color: green;
    }
  `],
  template: `
    <div appPadding>
      <md-card>
        <md-card-subtitle>I am a person</md-card-subtitle>
        <md-card-title>{{user.name}}</md-card-title>
        <md-card-content>
          <p>
          {{ user.dob | date:"dd/MMM/yyyy" }}
          </p>
        </md-card-content>
      </md-card>
    </div>
  `,
})
export class UserComponent implements OnInit {
  private user = {};

  constructor(
    private route: ActivatedRoute,
    private _usersService: UsersService,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = +params['id'];
      this.user = this._usersService.getUser(id);
    });
  }
}
