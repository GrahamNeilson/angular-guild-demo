import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { UsersComponent } from './users.component';
import { UserComponent } from './user.component';
import { UsersService } from './users.service';

@NgModule({
  declarations: [
    UsersComponent,
    UserComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: UsersComponent
      },
      {
        path: ':id',
        component: UserComponent
      }
    ])
  ],
  providers: [
    UsersService,
  ],
})
export class UsersModule { }
