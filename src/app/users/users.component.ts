import { Component } from '@angular/core';

import { UsersService } from './users.service';
import { Router } from '@angular/router';

@Component({
  selector: `app-users`,
  template: `
    <div appPadding>
      <md-card>
        <md-card-title>{{title}}</md-card-title>
        <md-card-content>
          <md-list>
            <md-list-item
              *ngFor="let user of users"
              (click)="select(user.id)">
                {{user.id}} : {{user.name | uppercase}}
            </md-list-item>
          </md-list>
        </md-card-content>
      </md-card>
    </div>
  `,
})
export class UsersComponent {
  title = 'Users';
  users = [];

  constructor(
    private _usersService: UsersService,
    private router: Router) {

    this.users = this._usersService.users;
  }

  select(id) {
    this.router.navigate([`/users`, id]);
  }
}
