import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { SharedModule } from './shared/shared.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home.component';
import { AboutComponent } from './about.component';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    SharedModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent,
      },
      {
        path: 'about',
        component: AboutComponent,
      },
      {
        path: 'users',
        loadChildren: 'app/users/users.module#UsersModule',
      },
      {
        path: 'pets',
        loadChildren: 'app/pets/pets.module#PetsModule',
      },
      {
        path: 'wiki',
        loadChildren: 'app/wiki/wiki.module#WikiModule',
      }
    ])
  ],
providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
