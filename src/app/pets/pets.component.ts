import {
  Component,
  ChangeDetectionStrategy,
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/core';

import { PetsService } from './pets.service';
import { Router } from '@angular/router';

/**
 * Animation, pipe, routing, directive
 */
@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('itemState', [
      state('inactive', style({
        transform: 'scale(1)'
      })),
      state('active',   style({
        transform: 'scale(1.3)'
      })),
      transition('inactive => active', animate('300ms ease-in')),
      transition('active => inactive', animate('300ms ease-out'))
    ])
  ],
  selector: `app-pets`,
  template: `
    <div appPadding>
      <md-card>
        <md-card-title>{{title}}</md-card-title>
        <md-card-content>
          <md-list>
            <md-list-item
              *ngFor="let pet of pets"
              (mouseover)="mouseoverPet(pet)"
              (mouseout)="mouseoutPet(pet)"
              (click)="select(pet.id)">
                <div [@itemState]="pet.state">
                {{pet.id}} : {{pet.name | uppercase}}
                </div>
            </md-list-item>
          </md-list>
        </md-card-content>
      </md-card>
    </div>
  `,
})
export class PetsComponent {
  title = 'Pets';
  pets = [];

  constructor(
    private _petsService: PetsService,
    private router: Router) {
    this.pets = this._petsService.pets;
  }

  select(id) {
    this.router.navigate([`/pets`, id]);
  }

  mouseoverPet(pet) {
    pet.state = 'active';
  }

  mouseoutPet(pet) {
    pet.state = 'inactive';
  }
}
