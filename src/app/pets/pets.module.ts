import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import { PetsComponent } from './pets.component';
import { PetComponent } from './pet.component';
import { PetViewComponent } from './pet-view.component';
import { PetsService } from './pets.service';

@NgModule({
  declarations: [
    PetsComponent,
    PetComponent,
    PetViewComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: PetsComponent
      },
      {
        path: ':id',
        component: PetComponent
      }
    ])
  ],
  providers: [
    PetsService,
  ],
})
export class PetsModule { }
