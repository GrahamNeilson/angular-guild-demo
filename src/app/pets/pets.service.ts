import { Injectable } from '@angular/core';

@Injectable()
export class PetsService {
  public pets: any;

  constructor() {
    this.pets = [
      { id: 11, active: 'inactive', greeting: 'Meow', name: 'biscuit'},
      { id: 12, active: 'inactive', greeting: 'Meow', name: 'meg'},
      { id: 13, active: 'inactive', greeting: 'Meow', name: 'scooter'},
      { id: 14, active: 'inactive', greeting: 'Woof', name: 'ollie'},
      { id: 15, active: 'inactive', greeting: 'Meow', name: 'dr teeth'},
      { id: 16, active: 'inactive', greeting: 'Meow', name: 'martha'},
      { id: 17, active: 'inactive', greeting: 'Meow', name: 'kipper'},
      { id: 18, active: 'inactive', greeting: 'Meow', name: 'cornelius'},
      { id: 18, active: 'inactive', greeting: 'Meow', name: 'caesar'},
    ];
  }

  getPet = id => this.pets.filter(pet => pet.id === id)[0];
}
