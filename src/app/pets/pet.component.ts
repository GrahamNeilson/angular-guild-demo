import { Component, OnInit } from '@angular/core';

import { PetsService } from './pets.service';
import { ActivatedRoute } from '@angular/router';

/**
 * Smart component - talks to service, parses route params, etc
 */
@Component({
  selector: `app-pet`,
  template: `
    <app-pet-view [pet]="pet" (emitter)="handleEmit($event)"><app-pet-view>
  `,
})
export class PetComponent implements OnInit {
  private pet = {};

  constructor(
    private route: ActivatedRoute,
    private _petsService: PetsService,
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = +params['id'];
      this.pet = this._petsService.getPet(id);
    });
  }

  handleEmit(event) {
    alert(event);
  }
}
