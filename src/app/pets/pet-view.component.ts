import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * Dumb component - mark up, css.
 */
@Component({
  selector: `app-pet-view`,
  styleUrls: [
    './pet.style.css',
    '../shared/styles/material.style.css'
  ],
  template: `
    <div appPadding>
      <md-card>
        <md-card-subtitle>I am a pet</md-card-subtitle>
        <md-card-title>{{pet.name}}</md-card-title>
        <md-card-content>
          <p>
          More info here
          </p>
        </md-card-content>
        <md-card-actions>
          <button md-button (click)="emitter.emit(pet.greeting)">Speak</button>
        </md-card-actions>
      </md-card>
    </div>
  `,
})
export class PetViewComponent {
  @Input()
  pet = {};

  @Output()
  emitter = new EventEmitter();
}
