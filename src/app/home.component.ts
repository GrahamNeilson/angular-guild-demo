import { Component } from '@angular/core';

@Component({
  selector: `app-home`,
  template: `
      <div appPadding>
        <md-card>
          <md-card-title>Home</md-card-title>
          <md-card-content>
            <p>
              Hello all.
            </p>
          </md-card-content>
        </md-card>
      </div>
  `,
})
export class HomeComponent {}
