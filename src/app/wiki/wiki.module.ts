import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { JsonpModule } from '@angular/http';

import { SharedModule } from '../shared/shared.module';
import { WikiComponent } from './wiki.component';
import { WikiViewComponent } from './wiki-view.component';
import { WikiService } from './wiki.service';

@NgModule({
  declarations: [
    WikiComponent,
    WikiViewComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    JsonpModule, // Required by wikipedia - jsonp requests
    RouterModule.forChild([
      {
        path: '',
        component: WikiComponent
      }
    ])
  ],
  providers: [
    WikiService,
  ],
})
export class WikiModule { }
