import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * Dumb
 */
@Component({
  selector: 'app-wiki-view',
  templateUrl: './wiki-view.template.html',
})
export class WikiViewComponent {
  @Input()
  results: any;

  @Output()
  value = new EventEmitter();
}
