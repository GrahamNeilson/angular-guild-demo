import { Injectable } from '@angular/core';
import { URLSearchParams, Jsonp } from '@angular/http';
import 'rxjs/add/operator/retry';
import { Observable } from 'rxJs/Observable';

@Injectable()
export class WikiService {

  constructor(private jsonp: Jsonp) {};

  search(term: string): Observable<any> {

    let wikiUrl = 'http://en.wikipedia.org/w/api.php';
    let params = new URLSearchParams();
    params.set('search', term); // the user's search value
    params.set('action', 'opensearch');
    params.set('format', 'json');
    params.set('callback', 'JSONP_CALLBACK');

    return this.jsonp
               .get(wikiUrl, { search: params })
               .retry(3)
               .map(response => <string[]> response.json()[1]);
  }
}
