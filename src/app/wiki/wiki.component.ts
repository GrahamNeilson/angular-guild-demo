import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/take';

import { WikiService } from './wiki.service';

/**
 * Smart
 */
@Component({
  selector: 'app-wiki',
  template: `
    <div appPadding>
      <app-wiki-view
        [results]="results | async"
        (value)=handleValue($event)>
      </app-wiki-view>
    </div>
`,
})
export class WikiComponent implements OnInit {
  term: any = new Subject();
  results: any;

  constructor(private _wikiService: WikiService) {}

  handleValue(value) {
    this.term.next(value);
  }

  ngOnInit() {
    this.results = this.term
                    .debounceTime(400)
                    .distinctUntilChanged()
                    .switchMap(term => this._wikiService.search(term));
  }
}
