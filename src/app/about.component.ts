import { Component } from '@angular/core';

@Component({
  selector: `app-about`,
  template: `
      <div appPadding>
        <md-card>
          <md-card-title>About</md-card-title>
          <md-card-content>
            <p>
              Hello again.
            </p>
          </md-card-content>
        </md-card>
      </div>
  `,
})
export class AboutComponent {}
