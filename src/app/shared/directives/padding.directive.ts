import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appPadding]',
})
export class PaddingDirective {
  constructor(private _el: ElementRef) {
    _el.nativeElement.style.padding = '16px';
  }
}
