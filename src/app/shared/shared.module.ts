import { NgModule } from '@angular/core';
/**
 * Modules shared by users, abilities, activites, home, core
 * Import them in order to export them, ie make available
 */

// "@angular2-material/core": "latest",
// "@angular2-material/list": "latest",
// "@angular2-material/card": "latest",
// "@angular2-material/icon": "latest",
// "@angular2-material/sidenav": "latest",
// "@angular2-material/toolbar": "latest",
// "@angular2-material/button": "latest",
// "@angular2-material/input": "latest",

import { CommonModule } from '@angular/common';
import { MdListModule } from '@angular2-material/list';
import { MdCardModule } from '@angular2-material/card';
import { MdIconModule, MdIconRegistry } from '@angular2-material/icon';
import { MdSidenavModule } from '@angular2-material/sidenav';
import { MdToolbarModule } from '@angular2-material/toolbar';
import { MdButtonModule } from '@angular2-material/button';
import { MdInputModule } from '@angular2-material/input';

import { PaddingDirective } from './directives';

@NgModule({
  declarations: [
    PaddingDirective,
  ],
  imports: [
    CommonModule,
    MdListModule,
    MdCardModule,
    MdIconModule,
    MdSidenavModule,
    MdToolbarModule,
    MdButtonModule,
    MdInputModule,
  ],
  exports: [
    CommonModule,
    MdListModule,
    MdCardModule,
    MdIconModule,
    MdSidenavModule,
    MdToolbarModule,
    MdButtonModule,
    MdInputModule,
    PaddingDirective,
  ],
  providers: [
    MdIconRegistry,
  ],
})
export class SharedModule {}
