import { AngularDemoTrialPage } from './app.po';

describe('angular-demo-trial App', function() {
  let page: AngularDemoTrialPage;

  beforeEach(() => {
    page = new AngularDemoTrialPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
